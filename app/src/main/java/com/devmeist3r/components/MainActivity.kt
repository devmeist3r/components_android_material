package com.devmeist3r.components

import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initChooseGenre()
    }

    fun initChooseGenre() {
        val names = listOf<String>("Masculino", "Feminino", "Outro", "Prefiro não informar")
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, names)
        etText.setAdapter(adapter)
    }
}